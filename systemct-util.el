(defun sudo-shell-command (command)
  (with-parsed-tramp-file-name "/sudo::" vec
    (tramp-send-command vec command)))

(defmacro with-tabulated-new-buffer (name body)
  `(let ((row-id (tabulated-list-get-id))
         (row (tabulated-list-get-entry)))
     (with-current-buffer
         (get-buffer-create
          (concat "*" ,name "-" row-id "*"))
       (erase-buffer)
       (insert ,body)
       (toggle-read-only)
       (switch-to-buffer (current-buffer)))))

(provide 'systemct-util)
