(require 'systemct-util)
(require 'json)

(defun force-string (x)
  (if (stringp x) x (format "%s" x)))

(define-derived-mode
  journalctl-mode
  tabulated-list-mode
  "Journalctl Mode"
  "Major Mode for reading log entries"

  (setq
   tabulated-list-entries
   (mapcar
    (lambda (x)
      (let-alist x
        (list
         .__CURSOR
         (vector
          (force-string .__REALTIME_TIMESTAMP)
          (force-string .__MONOTONIC_TIMESTAMP)
          (force-string ._BOOT_ID)
          (force-string ._UID)
          (force-string ._GID)
          (force-string ._SYSTEMD_SLICE)
          (force-string ._MACHINE_UD)
          (force-string ._HOSTNAME)
          (force-string .PRIORITY)
          (force-string ._CAP_EFFECTIVE)
          (force-string .SYSLOG_FACILITY)
          (force-string ._TRANSPORT)
          (force-string .SYSLOG_IDENTIFIER)
          (force-string ._COMM)
          (force-string ._EXE)
          (force-string ._PID)
          (force-string ._CMDLINE)
          (force-string ._SYSTEMD_CGROUP)
          (force-string ._SYSTEMD_UNIT)
          (force-string ._SOURCE_REALTIME_TIMESTAMP)
          (if (stringp .MESSAGE)
              .MESSAGE
            (apply #'concat
             (mapcar
              #'byte-to-string
              .MESSAGE)))))))
    (journalctl-json "minecraftd.service")))
  (setq tabulated-list-format
        [("REALTIME_TIMESTAMP" 20 t)
         ("MONOTONIC_TIMESTAMP" 15 t)
         ("BOOT_ID" 15 t)
         ("UID" 15 t)
         ("GID" 15 t)
         ("SYSTEMD_SLICE" 15 t)
         ("MACHINE_ID" 15 t)
         ("HOSTNAME" 15 t)
         ("PRIORITY" 15 t)
         ("CAP_EFFECTIVE" 15 t)
         ("SYSLOG_FACILITY" 15 t)
         ("TRANSPORT" 15 t)
         ("SYSLOG_IDENTIFIER" 15 t)
         ("COMM" 15 t)
         ("EXE" 15 t)
         ("PID" 15 t)
         ("CMDLINE" 15 t)
         ("SYSTEMD_CGROUP" 15 t)
         ("SYSTEMD_UNIT" 15 t)
         ("SOURCE_REALTIME_TIMESTAMP" 15 t)
         ("Message" 45 t)])
  (tabulated-list-init-header))

(defun journalctl-service ()
  (interactive)
  (with-current-buffer
      (get-buffer-create "*journalctl-temp*")
    (journalctl-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))))

(defun journalctl ()
  (interactive)
  (with-tabulated-new-buffer
   "journalctl"
   (mapcar
    (lambda (x)
      (let-alist (json-read-from-string x)
        .MESSAGE))
    (butlast
     (split-string
      (shell-command-to-string
       (concat
        "journalctl -o json --no-pager _SYSTEMD_UNIT="
        row-id))
      "\n")))))

(defun journalctl-json (service)
  (mapcar
   #'json-read-from-string
   (butlast
    (split-string
     (shell-command-to-string
      (concat
       "journalctl -o json --no-pager _SYSTEMD_UNIT="
       service))
     "\n"))))

(provide 'systemct-journal)

; __CURSOR  __REALTIME_TIMESTAMP  __MONOTONIC_TIMESTAMP _BOOT_ID _UID _GID _SYSTEMD_SLICE _MACHINE_UD _HOSTNAME PRIORITY _CAP_EFFECTIVE SYSLOG_FACILITY _TRANSPORT SYSLOG_IDENTIFIER _COMM _EXE MESSAGE _PID _CMDLINE _SYSTEMD_CGROUP _SYSTEMD_UNIT _SOURCE_REALTIME_TIMESTAMP
