(require 'dash)
(require 'tramp-sh)

(require 'systemct-util)
(require 'systemct-journal)

(defun systemctl-get-services ()
  "Return the service information from systemctl"
  (mapcar
   #'systemctl-parse-line
   (-take-while
    (lambda (x) (not (s-prefix? "LOAD" x)))
    (-filter
     (lambda (x) (not (string= x "")))

     (cdr
      (split-string
       (shell-command-to-string "systemctl --no-pager") "\n"))))))

(defun systemctl-parse-line (x)
  (letrec
      ((words
        (split-string
         (if (s-prefix? "●" x)
             (substring x 1)
           x)))
       (command (car words))
       (load (cadr words))
       (activity (caddr words))
       (status (cadddr words))
       (description (cddddr words)))
    (list
     command
     (vector
      command
      load
      activity
      status
      (s-join " " description)))))

(defvar systemctl-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map tabulated-list-mode-map)
    (define-key map (kbd "RET") 'systemctl-inspect)
    (define-key map (kbd "l") 'journalctl)
    (define-key map (kbd "r") 'systemctl-restart)
    (define-key map (kbd "s") 'systemctl-toggle)
    map))

(setq systemctl-highlights
      '(("running" . dired-flagged-face)
        ("failed" . dired-warning-face)
        ("waiting\\|plugged\\|mounted\\|exited\\|listening" . dired-ignored-face)))

(define-derived-mode
  systemctl-mode
  tabulated-list-mode
  "Systemctl Mode"
  "Major mode for controlling systemctl"

  (setq font-lock-defaults '(systemctl-highlights))
  (setq tabulated-list-entries #'systemctl-get-services)
  (setq tabulated-list-format
        [("UNIT"    36  t)
        ("LOAD"       9  t)
        ("ACTIVE"    10  t)
        ("SUB"      15  t)
        ("DESCRIPTION"  0  t)])
  (add-hook
   'tabulated-list-revert-hook
   (lambda ()
     (setq tabulated-list-entries
           (systemctl-get-services))))

  (tabulated-list-init-header))

(defun systemctl-list ()
  (interactive)
  (with-current-buffer (get-buffer-create "*systemctl-list*")
    (systemctl-mode)
    (tabulated-list-print)
    (switch-to-buffer (current-buffer))))

(defun systemctl-inspect ()
  (interactive)
  (with-tabulated-new-buffer
   "systemctl-status"
   (shell-command-to-string
    (string-join
     (list "systemctl --no-pager status " row-id)))))

(defun systemctl-toggle ()
  (interactive)
  (let ((row (tabulated-list-get-entry)))
    (sudo-shell-command
     (cond
      ((string= "running" (elt row 3))
       (concat "systemctl stop " (elt row 0)))
      (t
       (concat "systemctl start " (elt row 0))))))
  (tabulated-list-revert))

(defun systemctl-restart ()
  (interactive)
  (let ((row-id (tabulated-list-get-id)))
    (sudo-shell-command
     (concat "systemctl restart " row-id)))
  (tabulated-list-revert))

(provide 'systemct)
